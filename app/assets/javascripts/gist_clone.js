window.GistClone = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},

  initialize: function() {
    GistClone.gists = new GistClone.Collections.Gists();
    GistClone.gists.fetch({
      success: function() {
        new GistClone.Routers.Gists($('#container'), GistClone.gists);
        Backbone.history.start();
      }
    });
  }
};

$(document).ready(function(){
  window.GistClone.initialize();
});