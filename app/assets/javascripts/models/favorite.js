GistClone.Models.Favorite = Backbone.Model.extend({
  urlRoot: function () {
    return "/gists/" + this.get('gist_id') + "/favorites"
  }
});