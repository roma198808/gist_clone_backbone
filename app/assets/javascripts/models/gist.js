GistClone.Models.Gist = Backbone.Model.extend({
  urlRoot: "/gists",

  parse: function(resp) {
    if (resp['favorites'].length > 0) {
      resp['favorites'] = new GistClone.Models.Favorite(resp['favorites'][0]);
    } else {
      resp['favorites'] = null;
    }
    resp['gistfiles'] = new GistClone.Collections.GistFiles(resp['gistfiles']);
    return resp;
  },

  toJSON: function() {
    var newValues = _.omit(this.attributes, "favorites");
    return newValues;
  }
});
