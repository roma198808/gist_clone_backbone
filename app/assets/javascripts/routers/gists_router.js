GistClone.Routers.Gists = Backbone.Router.extend({
  initialize: function(rootEl, collection) {
    this.$rootEl = rootEl;
    this.collection = collection;
  },

  routes: {
    '': 'index',
    'gists/new': 'newGist'
  },

  index: function() {
    var view = new GistClone.Views.GistsIndex({
      collection: this.collection
    });
    this.$rootEl.html(view.render().$el);
  },

  newGist: function() {
    var view = new GistClone.Views.GistForm();
    this.$rootEl.html(view.render().$el);
  }

});
