GistClone.Views.TagsIndex = Backbone.View.extend({
  template: JST['tags/index'],

  render: function(){
    console.log(this.collection);
    var renderedContent = this.template({
      tags: this.collection
    });
    this.$el.html(renderedContent);
    return this;
  }

});