GistClone.Views.GistsIndex = Backbone.View.extend({

  initialize: function() {
    this.listenTo(this.collection, "all", this.render);
  },

  template: JST['gists/index'],

  render: function () {
    var renderedContent = this.template();
    this.$el.html(renderedContent);

    var $ul = this.$el.find('ul');

    this.collection.each(function(gist) {
      var detailView = new GistClone.Views.GistsShow({
        model: gist
      });
      $ul.append(detailView.render().$el);
    })


    return this;
  }

});
