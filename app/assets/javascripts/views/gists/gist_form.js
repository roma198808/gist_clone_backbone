GistClone.Views.GistForm = Backbone.View.extend({

  template: JST['gists/form'],

  events: {
    "submit form": "saveGist",
    "click #add_gistfile_form": "addGistfileForm"
  },

  render: function () {
    this.$el.html(this.template());
    new TextSearchSelect(this.$el.find('#search_text_box'));
    return this;
  },

  addGistfileForm: function (event) {
    event.preventDefault();

    var gistFile = new GistClone.Views.GistFileForm();
    this.$el.find('#gistFile').html(gistFile.render().$el);
  },

  saveGist: function(event) {
    event.preventDefault();

    var formData = $(event.currentTarget).serializeJSON();

    if (formData.gistfile == undefined) {
      var data = formData.gist;
    } else {
      var data = _.extend(formData.gist, { gistfiles: formData.gistfile });
    }

    GistClone.gists.create(data, {
      success: function(model) {
        Backbone.history.navigate("#/");
      }
    });
  }

});