GistClone.Views.GistsShow = Backbone.View.extend({

  template: JST['gists/show'],

  events: {
    "click button.favorite": "favorite",
    "click button.unfavorite": "unfavorite"
  },

  render: function () {
    var renderedContent = this.template({
      gist: this.model
    });
    this.$el.html(renderedContent);
    return this;
  },

  favorite: function(event) {
    var favoriteModel = new GistClone.Models.Favorite({
      'gist_id': this.model.id
    });
    var that = this;
    favoriteModel.save({}, {
      success: function(model, resp, options) {
        var $div = $(event.currentTarget).closest('div');
        that.model.set('favorites', model);
        $div.removeClass('can-favorite');
      }
    })
  },

  unfavorite: function(event) {
    var favoriteModel = this.model.get('favorites');
    favoriteModel.destroy({
      success: function() {
        var $div = $(event.currentTarget).closest('div');
        $div.addClass('can-favorite');
      }
    })
  }

});
