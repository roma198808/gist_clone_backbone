GistClone.Views.GistFileForm = Backbone.View.extend({

  template: JST['gistfiles/form'],

  render: function () {
    this.$el.html(this.template());
    return this;
  }

});
