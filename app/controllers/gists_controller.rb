class GistsController < ApplicationController
  def index
    @gists = current_user.gists
    render json: @gists
  end

  def create
    @gist = current_user.gists.new(params[:gist])
    @gist.gistfiles.new(params[:gistfiles]) if params[:gistfiles]
    if @gist.save
      render json: @gist
    else
      render json: nil, status: 422
    end
  end
end