class FavoritesController < ApplicationController

  def index
    favorites = current_user.favorites
    render :json => favorites
  end

  def create
    favorite = Favorite.new(gist_id: params[:gist_id], user_id: current_user.id)
    if favorite.save
      render :json => favorite
    else
      render :json => nil, status: 422
    end
  end

  def destroy
    favorite = current_user.favorites.find(params[:id])
    favorite.destroy
    render :json => favorite
  end

end
