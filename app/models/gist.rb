class Gist < ActiveRecord::Base
  attr_accessible :title, :user_id

  belongs_to :user
  has_many :favorites
  has_many :gistfiles
  has_many :taggings
  has_many :tags, through: :taggings

  def as_json(options)
    normal_hash = super(options)
    normal_hash.merge({ gistfiles: self.gistfiles, favorites: self.favorites })
  end
end
