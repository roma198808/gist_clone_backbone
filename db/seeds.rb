# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!(username: "Admin", password: "password")
# User.create!(username: "Sennacy", password: "catcat")
# User.create!(username: "Rocky", password: "catcat")
# User.create!(username: "Prins", password: "catcat")

Gist.create!(title: "Title 1", user_id: 1)
Gist.create!(title: "Title 2", user_id: 1)
Gist.create!(title: "Title 3", user_id: 1)
Gist.create!(title: "Title 4", user_id: 1)
Gist.create!(title: "Title 5", user_id: 1)