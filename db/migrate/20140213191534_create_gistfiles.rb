class CreateGistfiles < ActiveRecord::Migration
  def change
    create_table :gistfiles do |t|
      t.string :name
      t.string :body
      t.integer :gist_id

      t.timestamps
    end

    add_index :gistfiles, :gist_id
  end
end
